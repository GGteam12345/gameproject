﻿using UnityEngine;
using UnityEngine.UI;


public class policeManController : MonoBehaviour {

    //publics
    public Text wintext;
    public GameObject HealthBar;
    public GameObject PassPortPrefab;
    public GameObject ObjektPozice;

    //privates
    private float timeLeft;
    private bool spadlNaZem;
    private HealthBarController healthBar;
    private GameObject passPortNew;
    private bool pasVytvoren;


    void Start()
    {
        pasVytvoren = false;
        timeLeft = 2f;
        spadlNaZem = false;
        healthBar = HealthBar.GetComponent<HealthBarController>();
        passPortNew = PassPortPrefab;
    }

    void Update()
    {
        if (spadlNaZem)
            timeLeft -= Time.deltaTime;

        if (timeLeft <= 0.0 && !pasVytvoren)
        {
            VytvorPassPort();
            wintext.text = "Pick up your PassPort!";
            gameObject.SetActive(false);
        }


    }

    void OnCollisionEnter(Collision other)
    {
        //Hledam otagovany Terrain, teren musi mit triger triger!
        if (other.gameObject.CompareTag("Ground"))
        {
            //Win text pouze pro kontrolu.

            wintext.text = "Poiceman succesfully dropped.";
            spadlNaZem = true;
        }
    }


    void VytvorPassPort()
    {
        GameObject VytvorPassPort = Instantiate(passPortNew,
            new Vector3(ObjektPozice.transform.position.x, ObjektPozice.transform.position.y + 0.5f, ObjektPozice.transform.position.z),
            transform.rotation) as GameObject;
        pasVytvoren = true;

    }

    public bool getMuzeChodit()
    {
        if(pasVytvoren) { return true; } else { return false; }
    }
}
