﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

namespace UnityStandardAssets.Characters.FirstPerson
{
    public class HracController : MonoBehaviour
    {

        //publics
        public GameObject Policista;
        public GameObject Hrac;
        public AudioClip zvukSebraniPasu;
        public string nextLvl;


        //privates
        private policeManController policeManController;
        private RigidbodyFirstPersonController RbFPSController;
        private AudioSource source;
        private bool sebralPas;
        private float timeLeft;

        void Awake()
        {
            source = GetComponent<AudioSource>();
        }

        void Start()
        {
            policeManController = Policista.GetComponent<policeManController>();
            RbFPSController = Hrac.GetComponent<RigidbodyFirstPersonController>();
            sebralPas = false;
            timeLeft = 3F;
        }

        // Update is called once per frame
        void Update()
        {
            if (policeManController.getMuzeChodit())
            {
                RbFPSController.movementSettings.ForwardSpeed = 500;
                RbFPSController.movementSettings.BackwardSpeed = 250;
            }
            if (sebralPas)
            {
                timeLeft -= Time.deltaTime;
                if (timeLeft <= 0.0)
                {
                    SceneManager.LoadScene(nextLvl);
                }
            }


        }

        void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.CompareTag("PassPort")) {
                sebralPas = true;
                Destroy(other.gameObject);
                source.PlayOneShot(zvukSebraniPasu, 1F);
            }
        }
    }
}