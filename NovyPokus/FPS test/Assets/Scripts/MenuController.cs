﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace UnityStandardAssets.Characters.FirstPerson
{

    public class MenuController : MonoBehaviour
    {
        //publics
        public GameObject RigidBodyFPSController;       // objekt, ze ktereho ziskame komponentu (script) - RigidbodyFirstPersonController
        public GameObject[] statyButton;
        public GameObject LoadGameMenu;
        public GameObject PauseMenu;
        public GameObject MainMenu;

        //privates
        private GameObject AimImage;
        private bool PauseMenuBool;
        private bool MainMenuBool;
        private bool LoadMenuBool;
        private bool startHry = true;

        private RigidbodyFirstPersonController RbFPSController;     // promenna do ktere ulozime komponentu (script) - RigidbodyFirstPersonController

        // Use this for initialization
        void Start()
        {
            MainMenu = GameObject.Find("Canvas/MainMenu");
            PauseMenu = GameObject.Find("Canvas/PauseMenu");
            AimImage = GameObject.Find("Canvas/AimImage");

            LoadGameMenu.SetActive(false);

            RbFPSController = RigidBodyFPSController.GetComponent<RigidbodyFirstPersonController>();        // ziskani komponenty 

            //Najdeme tlacitka pro MainMenu a priradime jim listenery
            //NewGame
            GameObject.Find("Canvas/MainMenu/NewGame").GetComponent<Button>().onClick.AddListener(delegate ()
            {
                SceneManager.LoadScene("Scene1_Syria");
                Debug.Log("New game / MM");
            });

            //LoadGame
            GameObject.Find("Canvas/MainMenu/LoadGame").GetComponent<Button>().onClick.AddListener(delegate ()
            {

                turnMainMenuOff();
                turnLoadMenuOn();
                Debug.Log("Load game / MM");
            });

            //QuitGame
            GameObject.Find("Canvas/MainMenu/QuitGame").GetComponent<Button>().onClick.AddListener(delegate ()
            {
                Application.Quit();
                Debug.Log("Quit game / MM");
            });

            //Najdeme tlacitka pro PauseMenu a priradime jim listenery
            //Main Menu
            GameObject.Find("Canvas/PauseMenu/MainMenu").GetComponent<Button>().onClick.AddListener(delegate ()
            {
                turnPauseMenuOff();
                turnMainMenuOn();
                Debug.Log("Main menu / PM");
            });

            //ResumeGame
            GameObject.Find("Canvas/PauseMenu/ResumeGame").GetComponent<Button>().onClick.AddListener(delegate ()
            {
                turnPauseMenuOff();
                Debug.Log("Resume game / PM");
            });

            //SaveGame
            GameObject.Find("Canvas/PauseMenu/SaveGame").GetComponent<Button>().onClick.AddListener(delegate ()
            {
                turnPauseMenuOff();
                Debug.Log("Save game / PM");
            });


            //QuitGame
            GameObject.Find("Canvas/PauseMenu/QuitGame").GetComponent<Button>().onClick.AddListener(delegate ()
            {
                Application.Quit();
                Debug.Log("Quit game / PM");
            });



            //LoadGame
            statyButton[0].GetComponent<Button>().onClick.AddListener(delegate ()
            {
                Debug.Log("Prepnuti do sceny Syrie - strelba na zed");
                SceneManager.LoadScene("Scene1_Syria");
            });

            statyButton[1].GetComponent<Button>().onClick.AddListener(delegate ()
            {
                Debug.Log("Prepnuti do sceny Turecko - strelba na zed");
                SceneManager.LoadScene("Scene2_Turkey");
            });

            statyButton[2].GetComponent<Button>().onClick.AddListener(delegate ()
            {
                Debug.Log("Prepnuti do sceny Bulharsko - strelba na zed");
                SceneManager.LoadScene("Scene3_Bulgaria");
            });

            statyButton[3].GetComponent<Button>().onClick.AddListener(delegate ()
            {
                Debug.Log("Prepnuti do sceny Rumunsko - strelba na zed");
                SceneManager.LoadScene("Scene4_Romania");
            });

            statyButton[4].GetComponent<Button>().onClick.AddListener(delegate ()
            {
                Debug.Log("Prepnuti do sceny Madarsko - strelba na zed");
                SceneManager.LoadScene("Scene5_Hungary");
            });

            statyButton[5].GetComponent<Button>().onClick.AddListener(delegate ()
            {
                Debug.Log("Prepnuti do sceny Slovensko - strelba na zed");
                SceneManager.LoadScene("Scene6_Slovakia");
            });

            statyButton[6].GetComponent<Button>().onClick.AddListener(delegate ()
            {
                Debug.Log("Prepnuti do sceny Cesla Republika - strelba na zed");
                SceneManager.LoadScene("Scene7_Czech");
            });

            statyButton[7].GetComponent<Button>().onClick.AddListener(delegate ()
            {
                Debug.Log("Prepnuti do sceny Nemecko - strelba na zed");
                SceneManager.LoadScene("Scene8_Germany");
            });


            
        }



        // Update is called once per frame
        void Update()
        {

            if (startHry)
            {
                turnAllMenusOff();
                Debug.Log("Turn all FUCKING menus off!");
                startHry = false;
            }
                // zobrazovani / skryvani menu pri stisknuti Esc klavesy
                if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (!PauseMenuBool && !MainMenuBool && !LoadMenuBool)
                {
                    turnPauseMenuOn();
                }
                else if (!PauseMenuBool && MainMenuBool)
                {
                    turnMainMenuOff();
                }
                else if (PauseMenuBool && !MainMenuBool)
                {
                    turnPauseMenuOff();
                }
                else
                {
                    turnPauseMenuOff();
                    turnMainMenuOff();
                    turnLoadMenuOff();
                }
            }
        }

        // metoda, ktera vypne MainMenu
        void turnMainMenuOff()
        {
            MainMenu.SetActive(false);  // skryti main menu
            AimImage.SetActive(true);   // zobrazeni aim.kurzoru
            MainMenuBool = false;
            Cursor.visible = false;     // skryti kurzoru mysi
            timeStart();                // zapnuti casu

            RbFPSController.mouseLook.SetCursorLock(true);      // nastaveni Lock Cursor na true, aby nebylo mozne se dostat mimo okno aplikace
        }

        void turnMainMenuOn()
        {
            MainMenu.SetActive(true);       // zobrazeni main menu
            AimImage.SetActive(false);      // skryti aim.kurzoru
            MainMenuBool = true;
            Cursor.visible = true;          // zobrazeni kurzoru mysi
            timeStop();                     // zastaveni casu

            RbFPSController.mouseLook.SetCursorLock(false);     // nastaveni Lock Cursor na false, aby bylo mozne pohybovat kurzorem v menu
        }

        void turnPauseMenuOff()
        {
            PauseMenu.SetActive(false);     // skryti pause menu
            AimImage.SetActive(true);       // zobrazeni aim.kurzoru
            PauseMenuBool = false;
            Cursor.visible = false;         // skryti kurzoru mysi
            timeStart();                    // zapnuti casu

            RbFPSController.mouseLook.SetCursorLock(true);      // nastaveni Lock Cursor na true, aby nebylo mozne se dostat mimo okno aplikace
        }

        void turnPauseMenuOn()
        {
            PauseMenu.SetActive(true);      // zobrazeni pause menu
            AimImage.SetActive(false);      // skryti aim.kurzoru
            PauseMenuBool = true;
            Cursor.visible = true;          // zobrazeni kurzoru mysi
            timeStop();                     // zastaveni casu

            RbFPSController.mouseLook.SetCursorLock(false);     // nastaveni Lock Cursor na false, aby bylo mozne se pohybovat kurzorem v menu
        }

        void turnLoadMenuOn()
        {
            LoadGameMenu.SetActive(true);
            LoadMenuBool = true;
            Cursor.visible = true;          // zobrazeni kurzoru mysi
            timeStop();                     // zastaveni casu
        }

        void turnLoadMenuOff()
        {
            LoadGameMenu.SetActive(false);
            LoadMenuBool = false;
            RbFPSController.mouseLook.SetCursorLock(true);
        }

        void turnAllMenusOff()
        {
            turnLoadMenuOff();
            turnMainMenuOff();
            turnPauseMenuOff();
        }

        // metoda ktera nastavi rychlost casu na 0 - tzn. cas stoji
        void timeStop()
        {
            Time.timeScale = 0;
        }

        // metoda ktera nastavi rychlost casu na 1 - tzn. cas bezi normalni rychlosti
        void timeStart()
        {
            Time.timeScale = 1;
        }
    }
}