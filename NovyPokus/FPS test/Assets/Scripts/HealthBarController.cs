﻿using UnityEngine;
using System.Collections;

public class HealthBarController : MonoBehaviour
{

    //publics
    public GameObject[] cubesFolder;
    public GameObject[] healthBarTiles;

    //privates
    private CubeController[] cubes;
    private int overAllHP;
    int Percnt10;
    int Percnt20;
    int Percnt30;
    int Percnt40;
    int Percnt50;
    int Percnt60;
    int Percnt70;
    int Percnt80;
    int Percnt90;
    int Percnt100;


    // Use this for initialization
    void Start()
    {
        cubes = new CubeController[cubesFolder.Length];
        findComponentsForCubes();
        overAllHP = getOverAllHP();
        setPercnt();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        overAllHP = getOverAllHP();
        setHealthBar();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {

            Debug.Log(getOverAllHP());
        }
    }

    public int getOverAllHP()
    {
        int tmp = 0;

        for (int i = 0; i < cubesFolder.Length; i++)
        {
            tmp += cubes[i].getCubeHP();
        }

        return tmp;
    }

    void findComponentsForCubes()
    {
        for (int i = 0; i < cubesFolder.Length; i++)
        {
            cubes[i] = cubesFolder[i].GetComponent<CubeController>();
        }
    }

    void setHealthBar()
    {
        if (overAllHP <= Percnt100 && overAllHP > Percnt90)
        {
            healthBarTiles[0].SetActive(false);
        }


        if (overAllHP <= Percnt90 && overAllHP > Percnt80)
        {
            healthBarTiles[1].SetActive(false);
        }

        if (overAllHP <= Percnt80 && overAllHP > Percnt70)
        {
            healthBarTiles[2].SetActive(false);
        }

        if (overAllHP <= Percnt70 && overAllHP > Percnt60)
        {
            healthBarTiles[3].SetActive(false);
        }

        if (overAllHP <= Percnt60 && overAllHP > Percnt50)
        {
            healthBarTiles[4].SetActive(false);
        }

        if (overAllHP <= Percnt50 && overAllHP > Percnt40)
        {
            healthBarTiles[5].SetActive(false);
        }

        if (overAllHP <= Percnt40 && overAllHP > Percnt30)
        {
            healthBarTiles[6].SetActive(false);
        }

        if (overAllHP <= Percnt30 && overAllHP > Percnt20)
        {
            healthBarTiles[7].SetActive(false);
        }

        if (overAllHP <= Percnt20 && overAllHP > Percnt10)
        {
            healthBarTiles[8].SetActive(false);
        }

        if (overAllHP <= Percnt10)
        {
            healthBarTiles[9].SetActive(false);
        }




    }

    void setPercnt()
    {
        Percnt100 = overAllHP - (overAllHP / 10) * 1;
        Percnt90 = overAllHP - (overAllHP / 10) * 2;
        Percnt80 = overAllHP - (overAllHP / 10) * 3;
        Percnt70 = overAllHP - (overAllHP / 10) * 4;
        Percnt60 = overAllHP - (overAllHP / 10) * 5;
        Percnt50 = overAllHP - (overAllHP / 10) * 6;
        Percnt40 = overAllHP - (overAllHP / 10) * 7;
        Percnt30 = overAllHP - (overAllHP / 10) * 8;
        Percnt20 = overAllHP - (overAllHP / 10) * 9;
        Percnt10 = overAllHP - (overAllHP / 10) * 10;
    }
}
