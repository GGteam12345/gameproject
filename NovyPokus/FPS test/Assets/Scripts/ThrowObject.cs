﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class ThrowObject : MonoBehaviour
{
    //Publics
    public GameObject projectileCihla;
    public GameObject projectileDite;
    public GameObject projectileBomba;

    public int pocetStrel;
    public Camera MyCamera;       //Kamera ze ktere ziskavame pozici na aim.
    public GameObject objektPozice;

    //Privates
    private float throwSpeed = 200000f; //rychlost strely
    private GameObject projectile;
    private Text mujText;
    private int Zbran;
    
    //Code
    void Start()
    {
        try
        {
            mujText = GameObject.Find("PocetZbyvajicichStrel1").GetComponent<Text>();
        } catch
        {
            Debug.Log("Chybi text pro pocet zbyvajicich strel!");
        }
        
        setPocetStrelText();
    }

    void Update()
    {
        projectile = vyberZbrane(); // pri kazdem frame updatu se provede kontrola zmacknuti klaves "1, 2, 3" a vybere se zbran(cihla, dite, bomba)
        
        Ray ray = MyCamera.ScreenPointToRay(new Vector3(135, 200, 0));
        Debug.DrawRay(ray.origin, ray.direction * 10, Color.yellow);

        if (Input.GetButtonDown("Fire1") && Time.timeScale == 1.0F)
        {
            if (pocetStrel > 0)
            {
                /*
                Když bude tlačítko fire1 zmáčknuto, bude vytvořena instance nového objektu
                `projectile`, který poletí vstříc novým zážitkům!
                */
                vytvorInstanciBullet();

                pocetStrel--;

                setPocetStrelText();
            }
        }
    }

    void vytvorInstanciBullet()             //metoda vytvoření instance nového `projectile`, který poletí vstříc novým zážitkům!
    {
        GameObject throwThis = Instantiate(projectile,
    /* objektPozice.transform.position */
    new Vector3(objektPozice.transform.position.x, objektPozice.transform.position.y + 0.5F, objektPozice.transform.position.z),  // zmena souradnice Y = posun nahoru / dolu
    transform.rotation) as GameObject;      //Instantiate(projectile, smerStrely,transform.rotation)as GameObject;
        throwThis.GetComponent<Rigidbody>().AddRelativeForce(new Vector3(0, 0, throwSpeed));
    }


    void setPocetStrelText()        // nastavi text s poctem strel
    {
        try
        {
            mujText.text = "Jeste ti zbyva: " + pocetStrel.ToString() + " strel";
        } catch
        {
            Debug.Log("Chybi text pro pocetStrel");
        }
        
    }

    GameObject vyberZbrane()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
            Zbran = 1;
        if (Input.GetKeyDown(KeyCode.Alpha2))
            Zbran = 2;
        if (Input.GetKeyDown(KeyCode.Alpha3))
            Zbran = 3;

        switch (Zbran)
        {
            case 1:
                return projectileCihla;
            case 2:
                return projectileDite;
            case 3:
                return projectileBomba;
            default:
                return projectileCihla;
        }
    }
}

