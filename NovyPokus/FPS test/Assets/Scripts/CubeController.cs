﻿using UnityEngine;
using System.Collections;

public class CubeController : MonoBehaviour
{

    //publics
    public Material HP100_51;
    public Material HP50_1;

    //privates
    private int HP;

    // Use this for initialization
    void Start()
    {
        HP = 100;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        setMaterial();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Bullet"))
        {
            HP -= 16;
        }
    }

    void setMaterial()
    {
        if (HP < 100 && HP >= 50)
        {
            GetComponent<Renderer>().material = HP100_51;
        }
        else if (HP <= 51 && HP > 0)
        {
            GetComponent<Renderer>().material = HP50_1;
        }
        else if (HP <= 0)
        {
            HP = 0;
            Destroy(gameObject, 1);
        }
    }

    public int getCubeHP()
    {
        return HP;
    }
}
